using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CreatorKitCode;

public class AddHealthEffect : UsableItem.UsageEffect
{
    public int healthAmount = 10;

    public override bool Use(CharacterData user) // override a function in parent with new definition
    {
        user.Stats.ChangeHealth(healthAmount);
        return true;
    }
}
